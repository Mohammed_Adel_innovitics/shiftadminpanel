import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {RouterModule,Routes} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';


import { AppComponent } from './app.component';
import { BackgroundComponent } from './background/background.component';
import { ListofbookingComponent } from './listofbooking/listofbooking.component';
import { PopupdetailsComponent } from './popupdetails/popupdetails.component';


const routes : Routes = [

{path : 'background',component : BackgroundComponent},
{path : 'list of booking',component : ListofbookingComponent},
{path : 'popupdetails',component : PopupdetailsComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    BackgroundComponent,
    ListofbookingComponent,
    PopupdetailsComponent
  ],
  imports: [
  	RouterModule.forRoot(routes),
  	BrowserAnimationsModule,
    HttpClientModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
  

    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
