import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListofbookingComponent } from './listofbooking.component';

describe('ListofbookingComponent', () => {
  let component: ListofbookingComponent;
  let fixture: ComponentFixture<ListofbookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListofbookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListofbookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
