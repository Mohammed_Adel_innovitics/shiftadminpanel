import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'shiftAdminPanel';

  public showHide(){
    if(document.getElementById("mySidenav").style.display == "block"){
      document.getElementById("mySidenav").style.display = "none";
    }else{
      document.getElementById("mySidenav").style.display = "block";
    }
    }
}
